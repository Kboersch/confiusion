import React from "react";
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
} from "reactstrap";
import { Link } from "react-router-dom";
import { LoadingComponent } from "./LoadingComponent";
import { baseUrl } from '../shared/baseUrl';



function RenderMenuItem({ dish }) {
  return (
    <Card>
      <Link to={`/menu/${dish.id}`}>
        <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
        <CardImgOverlay>
          <CardTitle>{dish.name}</CardTitle>
        </CardImgOverlay>
      </Link>
    </Card>
  );
}
const MenuComponent = (props) => {
  const menu = props.dishes.dishes.map((dish) => {
    return (
      <div key={dish.id} className="col-12 col-md-5 m-1">
        <RenderMenuItem dish={dish} />
      </div>
    );
  });

  if (props.dishes.isLoading) {
    return (
      <div className="container">
        <div className="row">
          <LoadingComponent />
        </div>
      </div>
    );
  } else if (props.dishes.errMess) {
    return (
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4>{props.dishes.errMess}</h4>
          </div>
        </div>
      </div>
    );
  } else
    return (
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to="/home">Home</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>Menu</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>Menu</h3>
            <hr />
          </div>
        </div>
        <div className="row">{menu}</div>
        <div className="row">
          <div className="col-12 col-md-5 m-1"></div>
        </div>
      </div>
    );
};

export default MenuComponent;

// voor functional

// import React, { Component } from "react";

// import { Card, CardImg, CardImgOverlay, CardTitle } from "reactstrap";

// // 1 verplaats data const DISHES.js
// // 2 impoteer de nieuwe bestand in app.js
// // 3 past state to in app.js class App extends Component {
// //   constructor(props) {
// //     super(props);
// //     this.state = {
// //       dishes: DISHES
// //     };
// //   }
// // 4 maak het in app.js bekend aan het component <Menu dishes={this.state.dishes} />
// // 5 maak van conts menu state naar props

// export default class MenuComponent extends Component {

//     // console.log('menucomponent contructor aangemaakt');
//  // }

//   // componentDidMount(){
//   //     console.log('menucomponent componentDidMount aangemaakt');
//   // }

//   render() {
//     const menu = this.props.dishes.map((dish) => {
//       return (
//         <div key={dish.id} className="col-12 col-md-5 m-1">
//           <Card onClick={() => this.props.onClick(dish.id)}>
//             <CardImg width="100%" src={dish.image} alt={dish.name} />
//             <CardImgOverlay>
//               <CardTitle>{dish.name}</CardTitle>
//             </CardImgOverlay>
//           </Card>
//         </div>
//       );
//     });

//     console.log("menucomponent render aangemaakt");

//     return (
//       <div className="container">
//         <div className="row">{menu}</div>
//         <div className="row">
//           <div className="col-12 col-md-5 m-1">

//           </div>
//         </div>
//       </div>
//     );
//   }
// }
