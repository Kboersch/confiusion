import React from "react";
import { Media } from "reactstrap";
import { baseUrl } from "../shared/baseUrl";

function RenderLeaderComponent(props) {
  const leader = props.leader; // of deze var weg en overal props voor

  return (
    <Media tag="li" key={leader.id} className="col-12 mt-5">
      <Media left middle>
        <Media object src={baseUrl + leader.image} alt={leader.name} />
      </Media>
      <Media body className="ml-5">
        <Media heading>{leader.name}</Media>
        <p>{leader.designation}</p>
        <p>{leader.description}</p>
      </Media>
    </Media>
  );
}

export default RenderLeaderComponent;
